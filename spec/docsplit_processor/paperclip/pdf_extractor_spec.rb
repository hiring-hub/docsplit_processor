require 'spec_helper'

module Paperclip
  RSpec.describe PdfExtractor do
    describe '#make' do
      subject(:run_pdf_extractor) do
        PdfExtractor.new(file, output: dir).make
      end

      let(:file) do
        double(
          'File',
          path: file_path,
          close!: nil,
          content_type: content_type
        )
      end

      let(:file_path) { "#{dir}/test.docx" }
      let(:pdf_path) { "#{dir}/test.pdf" }
      let(:dir) { './temp_dir' }
      let(:content_type) do
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      end

      before do
        allow(Docsplit).to receive(:extract_pdf).with(file_path, output: dir)
        allow(File).to receive(:open).with(pdf_path)
      end

      it 'converts any format files to pdfs' do
        expect(Docsplit).to receive(:extract_pdf).with(file_path, output: dir)

        run_pdf_extractor
      end

      it 'returns a file' do
        expect(File).to receive(:open).with(pdf_path)

        run_pdf_extractor
      end

      context 'when the CV is already a pdf' do
        let(:file_path) { "#{dir}/test.pdf" }
        let(:content_type) { 'application/pdf' }

        it 'returns the original document' do
          expect(File).to receive(:open).with(file_path)

          run_pdf_extractor
        end

        it 'does not process the document with docsplit' do
          expect(Docsplit).not_to receive(:extract_pdf).with(any_args)

          run_pdf_extractor
        end
      end

      context 'when docsplit fails' do
        before do
          allow(Docsplit).to receive(:extract_pdf)
            .with(file_path, output: dir).and_raise(StandardError)
        end

        it 'closes the file and raises a paperclip error' do
          expect(file).to receive(:close!)
          expect { run_pdf_extractor }.to raise_error(Paperclip::Error)
        end
      end
    end
  end
end
